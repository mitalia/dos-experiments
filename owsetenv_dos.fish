#!/usr/bin/fish
echo Open Watcom Build Environment
set -x PATH $HOME/Sync/watcom/binl $PATH
set -x INCLUDE "$INCLUDE"
set -x INCLUDE $HOME/Sync/watcom/h:$INCLUDE
set -x WATCOM $HOME/Sync/watcom
set -x EDPATH $HOME/Sync/watcom/eddat
set -x WIPFC $HOME/Sync/watcom/wipfc
