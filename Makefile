all: pixel.exe

pixel.exe: pixel.c
	owcc -O3 -bdos -mcmodel=l pixel.c -opixel.exe

clean:
	rm -f pixel.exe pixel.o

run: pixel.exe
	dosbox pixel.exe
