#include <stdio.h>
#include <stdlib.h>
#include <dos.h>
#include <conio.h>

#define VIDEO_INT           0x10      /* the BIOS video interrupt. */
#define SET_MODE            0x00      /* BIOS func to set the video mode. */
#define VGA_256_COLOR_MODE  0x13      /* use to set 256-color mode. */
#define TEXT_MODE           0x03      /* use to set 80x25 text mode. */

#define SCREEN_WIDTH        320       /* width in pixels of mode 0x13 */
#define SCREEN_HEIGHT       200       /* height in pixels of mode 0x13 */
#define NUM_COLORS          256       /* number of colors in mode 0x13 */

typedef unsigned char  byte;
typedef unsigned short word;
typedef unsigned long  dword;


byte *VGA=(byte *)0xA0000000L;        /* this points to video memory. */
word *my_clock=(word *)0x0000046C;    /* this points to the 18.2hz system
                                         clock. */

/**************************************************************************
 *  set_mode                                                              *
 *     Sets the video mode.                                               *
 **************************************************************************/

void set_mode(byte mode) {
    union REGS regs;

    regs.h.ah = SET_MODE;
    regs.h.al = mode;
    int86(VIDEO_INT, &regs, &regs);
}

/**************************************************************************
 *  plot_pixel_fast                                                       *
 *    Plot a pixel by directly writing to video memory.                   *
 **************************************************************************/

void plot_pixel_fast(int x,int y,byte color) {
    VGA[y*SCREEN_WIDTH+x]=color;
}

char font[]=
    " 11 "
    "1  1"
    "1  1"
    "1  1"
    " 11 "

    " 11 "
    "  1 "
    "  1 "
    "  1 "
    "1111"

    " 11 "
    "1  1"
    "  1 "
    " 1  "
    "1111"

    "111 "
    "   1"
    " 11 "
    "   1"
    "111 "

    "  1 "
    " 1  "
    "1 1 "
    "1111"
    "  1 "

    "1111"
    "1   "
    "111 "
    "   1"
    "111 "

    " 11 "
    "1   "
    "111 "
    "1  1"
    " 11 "

    "1111"
    "   1"
    "  1 "
    " 1  "
    " 1  "

    " 11 "
    "1  1"
    " 11 "
    "1  1"
    " 11 "

    " 11 "
    "1  1"
    " 111"
    "   1"
    " 11 "

    " 11 "
    "1  1"
    "1111"
    "1  1"
    "1  1"

    "111 "
    "1  1"
    "111 "
    "1  1"
    "111 "

    " 111"
    "1   "
    "1   "
    "1   "
    " 111"

    "111 "
    "1  1"
    "1  1"
    "1  1"
    "111 "

    "1111"
    "1   "
    "111 "
    "1   "
    "1111"

    "1111"
    "1   "
    "111 "
    "1   "
    "1   "
    ;

byte *get_digit(int n) {
    return (byte *)font + 4*5*n;
}

void plot_digit(int digit, int xl, int yl, int color) {
    int x, y;
    byte *dg = get_digit(digit);
    for(y = 0; y < 5; ++y) {
        for(x=0; x < 4; ++x) {
            if(dg[y*4+x] != ' ') {
                plot_pixel_fast(xl+x, yl+y, color);
            }
        }
    }
}

int setup_mouse() {
    union REGS regs;
    regs.x.ax = 0;
    int86(0x33, &regs, &regs);
    return !!regs.x.ax;
}

unsigned get_mouse_status(int *x, int *y) {
    union REGS regs;
    regs.x.ax=3;
    int86(0x33, &regs, &regs);
    *x = regs.x.cx;
    *y = regs.x.dx;
    return regs.x.bx;
}

int main() {
    int rw = SCREEN_WIDTH/16;
    int rh = SCREEN_HEIGHT/16;
    int i, j, x,y;
    int last_offset = -1;
    int mouse_on;
    set_mode(VGA_256_COLOR_MODE);
    mouse_on = setup_mouse();

    while(kbhit() == 0) {
        int offset = (*my_clock)>>1;
        int mx, my;
        if(mouse_on && (get_mouse_status(&mx, &my) & 1)) {
            mx = mx >> 1;
            mx = 320 - mx;
            my = 200 - my;
            mx /= rw;
            my /= rh;
            offset = my*16+mx;
        }
        if(offset == last_offset) {
            _asm("hlt");
            continue;
        }
        last_offset = offset;
        for(i = 0; i < 16; ++i) {
            for(j = 0; j < 16; ++j) {
                byte color = ((i<<4) | j) + offset;
                for(y = i*rh; y < (i+1)*rh; ++y) {
                    for(x = j*rw; x < (j+1)*rw; ++x) {
                        plot_pixel_fast(x, y, color);
                    }
                }
                plot_digit(color >> 4, j*rw+1, i*rh+1, ~color);
                plot_digit(color & 15, j*rw+1+6, i*rh+1, ~color);
            }
        }
    }
    set_mode(TEXT_MODE);                /* set the video mode back to
                                         text mode. */
    return 0;
}
